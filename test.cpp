#include <iostream>
#include <cassert>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <thread>
#include <atomic>

#include "Spinlock.h"

class barrier {
public:
    explicit barrier(size_t num_threads) : barrier_size_(num_threads) {
    }
    void PassThrough() {
        std::unique_lock<std::mutex> lock(mutex_);

        num_threads_++;

        if (num_threads_ != barrier_size_) {
            while(!start_) {
                cv_.wait(lock);
            }
        } else {
            cv_.notify_all();
            start_ = true;
        }
    }

private:
    bool start_ = false;
    std::condition_variable cv_;
    size_t num_threads_ = 0;
    std::mutex mutex_;
    size_t barrier_size_;
};


void simple_test() {

    size_t test_num = 1;
    std::string str = " test passed\n";

    spinlock::SpinLock spinlock;

    spinlock.lock();
    spinlock.unlock();

    std::cout << test_num << str;
    test_num++;

    spinlock.lock();
    spinlock.unlock();
    spinlock.lock();
    spinlock.unlock();

    std::cout << test_num << str;
    test_num++;

    assert(spinlock.try_lock());
    assert(!spinlock.try_lock());
    spinlock.unlock();
    assert(spinlock.try_lock());
    spinlock.unlock();
    spinlock.lock();
    assert(!spinlock.try_lock());
    spinlock.unlock();

    std::cout << test_num << str;

}

void stress_test(size_t cnt_lock_threads, size_t cnt_trylock_threads, size_t cnt_loop) {
    std::vector<std::thread> threads;

    size_t cnt_threads = cnt_lock_threads + cnt_trylock_threads;
    barrier start_barrier(cnt_threads);
    std::atomic<bool> in_critical_section = false;
    spinlock::SpinLock spinlock;

    for (size_t i = 0; i < cnt_lock_threads; i++) {
        threads.emplace_back([&spinlock, &start_barrier, cnt_loop,
                                     &in_critical_section]() {
            start_barrier.PassThrough();

            for (size_t j = 0; j < cnt_loop; j++) {
                spinlock.lock();
                assert(!in_critical_section.exchange(true));
                std::this_thread::yield();
                assert(in_critical_section.exchange(false));
                spinlock.unlock();
            }
        });
    }
    for (size_t i = 0; i < cnt_trylock_threads; i++) {
        threads.emplace_back([&spinlock, &start_barrier, cnt_loop,
                                     &in_critical_section]() {
            start_barrier.PassThrough();

            for (size_t j = 0; j < cnt_loop; j++) {
                while(!spinlock.try_lock()) {
                    std::this_thread::yield();
                }
                assert(!in_critical_section.exchange(true));
                std::this_thread::yield();
                assert(in_critical_section.exchange(false));
                spinlock.unlock();
            }
        });
    }

    for (auto& thread : threads) {
        thread.join();
    }
}



int main() {
        std::cout << "simple test\n";
        simple_test();
        std::cout << "simple test passed\n";
        size_t n = std::thread::hardware_concurrency();
        size_t k = n / 3;
        stress_test(n - k, k, 100000);
        std::cout << "stress test passed\n";
}

