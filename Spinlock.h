#ifndef SPINLOCK_SPINLOCK_H
#define SPINLOCK_SPINLOCK_H

#include <atomic>
#include <thread>


namespace spinlock {

    class SpinLock {
    public:
        SpinLock() : locked_(0){};
        ~SpinLock() {
            assert(!locked_.load(std::memory_order_relaxed));
        }
        void lock() {
            while (locked_.exchange(1, std::memory_order_acquire)) {
                std::this_thread::yield();
            }
        }

        void unlock() {
            locked_.store(0, std::memory_order_release);
        }

        bool try_lock() {
            size_t unlocked = 0;
            return locked_.compare_exchange_strong(unlocked, 1, std::memory_order_acquire);
        }


    private:
        std::atomic<size_t> locked_;
    };

}



#endif //SPINLOCK_SPINLOCK_H
